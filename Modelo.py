#Classe Livro, contendo titulo, autor e registro
class Livro:
    def __init__(self, titulo, autor, registro):
        self.titulo = titulo
        self.autor = autor
        self.registro = registro
        self.disponivel = True

#Classe Biblioteca, para adicionar livros (buscar pelo autor e pelo título do livro)
class Biblioteca:
    def __init__(self):
        self.livros = []

    def adicionar_livro(self, livro):
        self.livros.append(livro)

    def buscar_livro_por_titulo(self, titulo):
        return [livro for livro in self.livros if livro.titulo.lower() == titulo.lower()]

    def buscar_livro_por_autor(self, autor):
        return [livro for livro in self.livros if livro.autor.lower() == autor.lower()]
    
#Condição para checar se possui algum livro com as característica indicadas pelo usuário
    def emprestar_livro(self, titulo):
        for livro in self.livros:
            if livro.titulo.lower() == titulo.lower() and livro.disponivel:
                livro.disponivel = False
                return livro
        return None
    
#Condição para chechar se o título do livro dita pelo usuário está disponível ou não
    def devolver_livro(self, titulo):
        for livro in self.livros:
            if livro.titulo.lower() == titulo.lower() and not livro.disponivel:
                livro.disponivel = True
                return livro
        return None
