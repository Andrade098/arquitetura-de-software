from Modelo import Biblioteca, Livro

class ControladorBiblioteca:
    def __init__(self):
        self.biblioteca = Biblioteca()

    def adicionar_livro(self, titulo, autor, registro):
        livro = Livro(titulo, autor, registro)
        self.biblioteca.adicionar_livro(livro)

    def buscar_livro_por_titulo(self, titulo):
        return self.biblioteca.buscar_livro_por_titulo(titulo)

    def buscar_livro_por_autor(self, autor):
        return self.biblioteca.buscar_livro_por_autor(autor)

    def emprestar_livro(self, titulo):
        return self.biblioteca.emprestar_livro(titulo)

    def devolver_livro(self, titulo):
        return self.biblioteca.devolver_livro(titulo)
