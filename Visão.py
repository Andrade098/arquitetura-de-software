from Controle import ControladorBiblioteca
#Esté é o Codigo Principal que fará a execução
class InterfaceUsuario:
    def __init__(self):
        self.controlador = ControladorBiblioteca()

#Funcão Principal 
    def menu_principal(self):
        while True:
            print("\n===== MENU PRINCIPAL =====")
            print("1. Adicionar novo livro")
            print("2. Buscar livro por título")
            print("3. Buscar livro por autor")
            print("4. Emprestar livro")
            print("5. Devolver livro")
            print("6. Sair")
            escolha = input("Escolha uma opção: ")

            if escolha == "1":
                self.adicionar_livro()
            elif escolha == "2":
                self.buscar_livro_por_titulo()
            elif escolha == "3":
                self.buscar_livro_por_autor()
            elif escolha == "4":
                self.emprestar_livro()
            elif escolha == "5":
                self.devolver_livro()
            elif escolha == "6":
                print("Saindo...")
                break
            else:
                print("Opção inválida. Por favor, escolha uma opção válida.")
                
#Funcões para Adicionar Livros, Buscar, Emprestar e Devolver 
    def adicionar_livro(self):
        titulo = input("Digite o título do livro: ")
        autor = input("Digite o autor do livro: ")
        registro = input("Digite o número de registro único do livro: ")
        self.controlador.adicionar_livro(titulo, autor, registro)
        print("Livro adicionado com sucesso!")

    def buscar_livro_por_titulo(self):
        titulo = input("Digite o título do livro: ")
        livros_encontrados = self.controlador.buscar_livro_por_titulo(titulo)
        if livros_encontrados:
            for livro in livros_encontrados:
                print(f"Título: {livro.titulo}, Autor: {livro.autor}, Registro: {livro.registro}")
        else:
            print("Nenhum livro encontrado com esse título.")

    def buscar_livro_por_autor(self):
        autor = input("Digite o autor do livro: ")
        livros_encontrados = self.controlador.buscar_livro_por_autor(autor)
        if livros_encontrados:
            for livro in livros_encontrados:
                print(f"Título: {livro.titulo}, Autor: {livro.autor}, Registro: {livro.registro}")
        else:
            print("Nenhum livro encontrado desse autor.")

    def emprestar_livro(self):
        titulo = input("Digite o título do livro a ser emprestado: ")
        livro = self.controlador.emprestar_livro(titulo)
        if livro:
            print(f"O livro '{livro.titulo}' foi emprestado com sucesso.")
        else:
            print("Livro não disponível para empréstimo.")

    def devolver_livro(self):
        titulo = input("Digite o título do livro a ser devolvido: ")
        livro = self.controlador.devolver_livro(titulo)
        if livro:
            print(f"O livro '{livro.titulo}' foi devolvido com sucesso.")
        else:
            print("Livro não encontrado para devolução.")

# Execução do programa
if __name__ == "__main__":
    interface = InterfaceUsuario()
    interface.menu_principal()
